<?php
/**
 * Created by PhpStorm.
 * User: sandra.poll
 * Date: 23.05.2019
 * Time: 10:45
 */
?>
@extends('master')

@section('content')
    <div class="full-height">
        <div class="content top">
            @isset($projects)

                @foreach($projects as $project)
                    <h1 class="display-4">{{ $project->title }}</h1>
                    <p>{{ $project->description }}</p>
                    <a href="{{ route('project.show', ['project' => $project->id]) }}"
                       class="btn btn-outline-info wide rounded-0">Edit</a>
                    <a href="{{ route('project.delete', ['project' => $project->id]) }}">
                        <button class="btn btn-outline-danger rounded-0" type="submit">Delete</button>
                    </a>
                    <hr>
                @endforeach
            @endisset

            @if (count($projects) === 0)
                <h2>There are no projects!</h2>
                <hr>
                <a href="/add">
                    <button class="btn btn-outline-secondary rounded-0" type="submit">Create new</button>
                </a>
            @endif
        </div>
    </div>
@endsection

